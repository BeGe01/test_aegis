import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:test_aegis/drawer.dart';

class PieChartData {
  PieChartData(this.merk, this.jumlah);
  final String merk;
  final int jumlah;
}

class PieChartPage extends StatelessWidget {
  // TooltipBehavior _tooltipPie = TooltipBehavior(enable: true);
  // TooltipBehavior _tooltipDoughnut = TooltipBehavior(enable: true);
  // TooltipBehavior _tooltipRadial = TooltipBehavior(enable: true);

  final List<PieChartData> chartData = [
    PieChartData('Samsung', 90),
    PieChartData('VIVO', 101),
    PieChartData('OPPO', 88),
    PieChartData('iPhone', 113),
    PieChartData('Xiaomi', 74),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pie Chart"),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [Color(0xff334192), Color(0xff432677)],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          )),
        ),
      ),
      drawer: DrawerWidget(),
      body: Container(
        margin: EdgeInsets.all(10),
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              child: Column(
                children: [
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCircularChart(
                      title: ChartTitle(
                        text: "Pie Chart\nMerk Handphone Yang Terjual",
                      ),
                      legend: Legend(
                          isVisible: true,
                          overflowMode: LegendItemOverflowMode.scroll),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <CircularSeries>[
                        PieSeries<PieChartData, String>(
                            dataSource: chartData,
                            xValueMapper: (PieChartData data, _) => data.merk,
                            yValueMapper: (PieChartData data, _) => data.jumlah,
                            explode: true,
                            explodeIndex: 1)
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCircularChart(
                      title: ChartTitle(
                        text: "Doughnut Chart\nMerk Handphone Yang Terjual",
                      ),
                      legend: Legend(
                          isVisible: true,
                          overflowMode: LegendItemOverflowMode.scroll),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <CircularSeries>[
                        DoughnutSeries<PieChartData, String>(
                          dataSource: chartData,
                          xValueMapper: (PieChartData data, _) => data.merk,
                          yValueMapper: (PieChartData data, _) => data.jumlah,
                        )
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCircularChart(
                      title: ChartTitle(
                        text: "Radial Bar Chart\nMerk Handphone Yang Terjual",
                      ),
                      legend: Legend(
                          isVisible: true,
                          overflowMode: LegendItemOverflowMode.scroll),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <CircularSeries>[
                        RadialBarSeries<PieChartData, String>(
                            dataSource: chartData,
                            xValueMapper: (PieChartData data, _) => data.merk,
                            yValueMapper: (PieChartData data, _) => data.jumlah,
                            maximumValue: 150)
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
