import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:test_aegis/drawer.dart';

class BarChartData {
  BarChartData(this.tahun, this.jumlah);
  final int tahun;
  final int jumlah;
}

class BarChartPage extends StatelessWidget {
  final List<BarChartData> chartData = [
    BarChartData(2015, 30),
    BarChartData(2016, 101),
    BarChartData(2017, 88),
    BarChartData(2018, 123),
    BarChartData(2019, 74),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bar Chart"),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [Color(0xff334192), Color(0xff432677)],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          )),
        ),
      ),
      drawer: DrawerWidget(),
      body: Container(
        margin: EdgeInsets.all(10),
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              child: Column(
                children: [
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCartesianChart(
                      primaryXAxis: NumericAxis(decimalPlaces: 0),
                      title: ChartTitle(
                        text: "Bar Chart\nPenjualan dari Tahun ke Tahun",
                      ),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <ChartSeries>[
                        BarSeries<BarChartData, int>(
                          dataSource: chartData,
                          xValueMapper: (BarChartData data, _) => data.tahun,
                          yValueMapper: (BarChartData data, _) => data.jumlah,
                        )
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCartesianChart(
                      primaryXAxis: NumericAxis(decimalPlaces: 0),
                      title: ChartTitle(
                        text: "Column Chart\nPenjualan dari Tahun ke Tahun",
                      ),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <ChartSeries>[
                        ColumnSeries<BarChartData, int>(
                          dataSource: chartData,
                          xValueMapper: (BarChartData data, _) => data.tahun,
                          yValueMapper: (BarChartData data, _) => data.jumlah,
                        )
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCartesianChart(
                      primaryXAxis: NumericAxis(decimalPlaces: 0),
                      title: ChartTitle(
                        text: "Spline Chart\nPenjualan dari Tahun ke Tahun",
                      ),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <ChartSeries>[
                        ScatterSeries<BarChartData, int>(
                          dataSource: chartData,
                          xValueMapper: (BarChartData data, _) => data.tahun,
                          yValueMapper: (BarChartData data, _) => data.jumlah,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
