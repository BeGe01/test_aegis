import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:test_aegis/register_page.dart';
import 'package:test_aegis/tabel_page.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key, required this.username, required this.password})
      : super(key: key);
  final String username;
  final String password;

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController usernameController = TextEditingController(text: null);
  TextEditingController passController = TextEditingController(text: null);

  SnackBar snackBar = SnackBar(
    content: Text('Username atau Password salah!'),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
          gradient: LinearGradient(
        colors: [Color(0xff334192), Color(0xff432677)],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
      )),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.all(10),
              child: Text(
                "LOGIN",
                style: TextStyle(
                    fontSize: 40,
                    color: Colors.transparent,
                    decoration: TextDecoration.underline,
                    decorationColor: Colors.white,
                    decorationThickness: 2,
                    shadows: [
                      Shadow(color: Colors.white, offset: Offset(0, -5))
                    ]),
              ),
            ),
            Container(
              margin: EdgeInsets.all(5),
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextField(
                controller: usernameController,
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.person),
                    hintText: "Username",
                    labelText: "Username",
                    enabledBorder: OutlineInputBorder(
                        // borderRadius: BorderRadius.circular(5),
                        borderSide:
                            BorderSide(color: Colors.purple, width: 2.0)),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.purple, width: 3.0)),
                    filled: true,
                    fillColor: Colors.grey[200]),
              ),
            ),
            Container(
              margin: EdgeInsets.all(5),
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextField(
                controller: passController,
                obscureText: true,
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.lock),
                    labelText: "Password",
                    enabledBorder: OutlineInputBorder(
                        // borderRadius: BorderRadius.circular(5),
                        borderSide:
                            BorderSide(color: Colors.purple, width: 2.0)),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.purple, width: 3.0)),
                    filled: true,
                    fillColor: Colors.grey[200]),
              ),
            ),
            Container(
              margin: EdgeInsets.all(5),
              child: ElevatedButton(
                  onPressed: () {
                    if (usernameController.text == widget.username &&
                        passController.text == widget.password) {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                        return TabelPage();
                      }));
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    }
                  },
                  child: Text("LOGIN"),
                  style: ElevatedButton.styleFrom(
                      minimumSize:
                          Size(MediaQuery.of(context).size.width * 0.8, 40),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)))),
            ),
            Container(
              margin: EdgeInsets.all(5),
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) {
                      return RegisterPage();
                    }));
                  },
                  child: Text("REGISTER"),
                  style: ElevatedButton.styleFrom(
                      side: BorderSide(color: Colors.white, width: 2),
                      backgroundColor: Colors.transparent,
                      shadowColor: Colors.transparent,
                      minimumSize:
                          Size(MediaQuery.of(context).size.width * 0.8, 40),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)))),
            ),
          ],
        ),
      ),
    ));
  }
}
