import 'package:flutter/material.dart';
import 'package:test_aegis/drawer.dart';

class TabelPage extends StatelessWidget {
  const TabelPage({Key? key}) : super(key: key);

  DataTable _createDataTable() {
    return DataTable(
      columns: _createColumns(),
      rows: _createRows(),
      border: TableBorder(horizontalInside: BorderSide(color: Colors.purple)),
      headingRowColor:
          MaterialStateProperty.resolveWith((states) => Colors.purple),
      headingTextStyle:
          TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
    );
  }

  List<DataColumn> _createColumns() {
    return [
      DataColumn(label: Text('ID')),
      DataColumn(label: Text('First Name')),
      DataColumn(label: Text('Last Name')),
    ];
  }

  List<DataRow> _createRows() {
    return [
      DataRow(cells: [
        DataCell(Text('1')),
        DataCell(Text('Atlanta')),
        DataCell(Text('Spehr')),
      ]),
      DataRow(cells: [
        DataCell(Text('2')),
        DataCell(Text('Atlanta')),
        DataCell(Text('Spehr')),
      ]),
      DataRow(cells: [
        DataCell(Text('3')),
        DataCell(Text('Atlanta')),
        DataCell(Text('Spehr')),
      ]),
      DataRow(cells: [
        DataCell(Text('4')),
        DataCell(Text('Atlanta')),
        DataCell(Text('Spehr')),
      ]),
      DataRow(cells: [
        DataCell(Text('5')),
        DataCell(Text('Atlanta')),
        DataCell(Text('Spehr')),
      ]),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Table"),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [Color(0xff334192), Color(0xff432677)],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          )),
        ),
      ),
      drawer: DrawerWidget(),
      body: Container(
        margin: EdgeInsets.all(10),
        child: ListView(
          children: [
            Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.purple, width: 2),
                    borderRadius: BorderRadius.circular(10)),
                child: _createDataTable())
          ],
        ),
      ),
    );
  }
}
