import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:test_aegis/drawer.dart';

class LineChartData {
  LineChartData(this.tahun, this.jumlah);
  final int tahun;
  final int jumlah;
}

class LineChartPage extends StatelessWidget {
  final List<LineChartData> chartData = [
    LineChartData(2015, 30),
    LineChartData(2016, 101),
    LineChartData(2017, 88),
    LineChartData(2018, 123),
    LineChartData(2019, 74),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Line Chart"),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [Color(0xff334192), Color(0xff432677)],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          )),
        ),
      ),
      drawer: DrawerWidget(),
      body: Container(
        margin: EdgeInsets.all(10),
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              child: Column(
                children: [
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCartesianChart(
                      title: ChartTitle(
                        text: "Line Chart\nPenjualan dari Tahun ke Tahun",
                      ),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <ChartSeries>[
                        LineSeries<LineChartData, int>(
                          dataSource: chartData,
                          xValueMapper: (LineChartData data, _) => data.tahun,
                          yValueMapper: (LineChartData data, _) => data.jumlah,
                        )
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCartesianChart(
                      title: ChartTitle(
                        text: "Area Chart\nPenjualan dari Tahun ke Tahun",
                      ),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <ChartSeries>[
                        AreaSeries<LineChartData, int>(
                          dataSource: chartData,
                          xValueMapper: (LineChartData data, _) => data.tahun,
                          yValueMapper: (LineChartData data, _) => data.jumlah,
                        )
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCartesianChart(
                      title: ChartTitle(
                        text: "Spline Chart\nPenjualan dari Tahun ke Tahun",
                      ),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <ChartSeries>[
                        SplineSeries<LineChartData, int>(
                          dataSource: chartData,
                          xValueMapper: (LineChartData data, _) => data.tahun,
                          yValueMapper: (LineChartData data, _) => data.jumlah,
                        )
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCartesianChart(
                      title: ChartTitle(
                        text: "Step line Chart\nPenjualan dari Tahun ke Tahun",
                      ),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <ChartSeries>[
                        StepLineSeries<LineChartData, int>(
                          dataSource: chartData,
                          xValueMapper: (LineChartData data, _) => data.tahun,
                          yValueMapper: (LineChartData data, _) => data.jumlah,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
