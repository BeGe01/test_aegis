import 'package:flutter/material.dart';
import 'package:test_aegis/line_chart_page.dart';
import 'package:test_aegis/pie_chart_page.dart';
import 'package:test_aegis/stacked_chart_page.dart';
import 'package:test_aegis/tabel_page.dart';

import 'bar_chart_page.dart';

class DrawerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _drawerHeader(),
          _drawerItem(
              icon: Icons.table_chart_rounded,
              text: 'Table',
              onTap: () => Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return TabelPage();
                  }))),
          _drawerItem(
              icon: Icons.line_axis_rounded,
              text: 'Line Chart',
              onTap: () => Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return LineChartPage();
                  }))),
          _drawerItem(
              icon: Icons.bar_chart_rounded,
              text: 'Bar Chart',
              onTap: () => Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return BarChartPage();
                  }))),
          _drawerItem(
              icon: Icons.pie_chart_rounded,
              text: 'Pie Chart',
              onTap: () => Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return PieChartPage();
                  }))),
          _drawerItem(
              icon: Icons.stacked_line_chart_rounded,
              text: 'Stacked Chart',
              onTap: () => Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return StackedChartPage();
                  }))),
        ],
      ),
    );
  }
}

Widget _drawerHeader() {
  return DrawerHeader(
      decoration: BoxDecoration(
          gradient: LinearGradient(
        colors: [Color(0xff334192), Color(0xff432677)],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
      )),
      child: Text(""));
}

Widget _drawerItem({IconData? icon, String? text, GestureTapCallback? onTap}) {
  return ListTile(
    title: Row(
      children: <Widget>[
        Icon(icon),
        Padding(
          padding: EdgeInsets.only(left: 25.0),
          child: Text(
            text!,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    ),
    onTap: onTap,
  );
}
