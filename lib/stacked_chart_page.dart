import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:test_aegis/drawer.dart';

class StackedChartData {
  StackedChartData(this.tahun, this.jumlah);
  final int tahun;
  final int jumlah;
}

class StackedChartPage extends StatelessWidget {
  final List<StackedChartData> penjualanVIVO = [
    StackedChartData(2015, 10),
    StackedChartData(2016, 30),
    StackedChartData(2017, 54),
    StackedChartData(2018, 21),
    StackedChartData(2019, 35),
  ];

  final List<StackedChartData> penjualanOPPO = [
    StackedChartData(2015, 21),
    StackedChartData(2016, 46),
    StackedChartData(2017, 72),
    StackedChartData(2018, 12),
    StackedChartData(2019, 31),
  ];

  final List<StackedChartData> penjualanIPHONE = [
    StackedChartData(2015, 13),
    StackedChartData(2016, 26),
    StackedChartData(2017, 39),
    StackedChartData(2018, 62),
    StackedChartData(2019, 103),
  ];

  final List<StackedChartData> penjualanSamsung = [
    StackedChartData(2015, 22),
    StackedChartData(2016, 45),
    StackedChartData(2017, 11),
    StackedChartData(2018, 23),
    StackedChartData(2019, 41),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Stacked Chart"),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [Color(0xff334192), Color(0xff432677)],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          )),
        ),
      ),
      drawer: DrawerWidget(),
      body: Container(
        margin: EdgeInsets.all(10),
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              child: Column(
                children: [
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCartesianChart(
                      primaryXAxis: CategoryAxis(),
                      title: ChartTitle(
                        text:
                            "Stacked Line Chart\nPenjualan Handphone setiap Tahun",
                      ),
                      legend: Legend(
                          isVisible: true,
                          overflowMode: LegendItemOverflowMode.scroll,
                          position: LegendPosition.bottom),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <ChartSeries>[
                        StackedLineSeries<StackedChartData, int>(
                          name: "VIVO",
                          dataSource: penjualanVIVO,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedLineSeries<StackedChartData, int>(
                          name: "OPPO",
                          dataSource: penjualanOPPO,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedLineSeries<StackedChartData, int>(
                          name: "Samsung",
                          dataSource: penjualanSamsung,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedLineSeries<StackedChartData, int>(
                          name: "IPhone",
                          dataSource: penjualanIPHONE,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCartesianChart(
                      primaryXAxis: CategoryAxis(),
                      title: ChartTitle(
                        text:
                            "Stacked Area Chart\nPenjualan Handphone setiap Tahun",
                      ),
                      legend: Legend(
                          isVisible: true,
                          overflowMode: LegendItemOverflowMode.scroll,
                          position: LegendPosition.bottom),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <ChartSeries>[
                        StackedAreaSeries<StackedChartData, int>(
                          name: "VIVO",
                          dataSource: penjualanVIVO,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedAreaSeries<StackedChartData, int>(
                          name: "OPPO",
                          dataSource: penjualanOPPO,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedAreaSeries<StackedChartData, int>(
                          name: "Samsung",
                          dataSource: penjualanSamsung,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedAreaSeries<StackedChartData, int>(
                          name: "IPhone",
                          dataSource: penjualanIPHONE,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCartesianChart(
                      primaryXAxis: CategoryAxis(),
                      title: ChartTitle(
                        text:
                            "Stacked Bar Chart\nPenjualan Handphone setiap Tahun",
                      ),
                      legend: Legend(
                          isVisible: true,
                          overflowMode: LegendItemOverflowMode.scroll,
                          position: LegendPosition.bottom),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <ChartSeries>[
                        StackedBarSeries<StackedChartData, int>(
                          name: "VIVO",
                          dataSource: penjualanVIVO,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedBarSeries<StackedChartData, int>(
                          name: "OPPO",
                          dataSource: penjualanOPPO,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedBarSeries<StackedChartData, int>(
                          name: "Samsung",
                          dataSource: penjualanSamsung,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedBarSeries<StackedChartData, int>(
                          name: "IPhone",
                          dataSource: penjualanIPHONE,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                      ],
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.all(10),
                    child: SfCartesianChart(
                      primaryXAxis: CategoryAxis(),
                      title: ChartTitle(
                        text:
                            "Stacked Column Chart\nPenjualan Handphone setiap Tahun",
                      ),
                      legend: Legend(
                          isVisible: true,
                          overflowMode: LegendItemOverflowMode.scroll,
                          position: LegendPosition.bottom),
                      tooltipBehavior: TooltipBehavior(enable: true),
                      series: <ChartSeries>[
                        StackedColumnSeries<StackedChartData, int>(
                          name: "VIVO",
                          dataSource: penjualanVIVO,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedColumnSeries<StackedChartData, int>(
                          name: "OPPO",
                          dataSource: penjualanOPPO,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedColumnSeries<StackedChartData, int>(
                          name: "Samsung",
                          dataSource: penjualanSamsung,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                        StackedColumnSeries<StackedChartData, int>(
                          name: "IPhone",
                          dataSource: penjualanIPHONE,
                          xValueMapper: (StackedChartData data, _) =>
                              data.tahun,
                          yValueMapper: (StackedChartData data, _) =>
                              data.jumlah,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
